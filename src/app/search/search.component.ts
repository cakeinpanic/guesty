import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DataService, Sort} from '../services/data.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  form: FormGroup;
  sort = Object.values(Sort);

  @Output() onSortChange = new EventEmitter<Sort>();
  @Output() onSearch = new EventEmitter<string>();

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl('', Validators.required),
      sort: new FormControl(Sort.Title, Validators.required),
    });

    this.form.controls['sort'].valueChanges.subscribe(newSort => {
      this.onSortChange.next(newSort);
    });

    this.dataService.paramsChanged.subscribe((newParams) => {
      this.form.patchValue({title: newParams.query, sortChange: newParams.sort});
    });

  }

  search() {
    const query = this.form.getRawValue().title;
    this.onSearch.next(query);
  }


}
