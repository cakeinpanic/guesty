import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { PreviewComponent } from './preview-list/preview/preview.component';
import { GiphyComponent } from './giphy/giphy.component';
import { PreviewListComponent } from './preview-list/preview-list.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    PreviewComponent,
    GiphyComponent,
    PreviewListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    CommonModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
