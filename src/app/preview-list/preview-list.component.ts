import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {IGif} from '../services/data.service';

@Component({
  selector: 'app-preview-list',
  templateUrl: './preview-list.component.html',
  styleUrls: ['./preview-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreviewListComponent implements OnInit {
  @Input() gifs: IGif[] = [];

  constructor() {
  }

  ngOnInit() {
  }
}
