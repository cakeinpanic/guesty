import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {IGif} from '../../services/data.service';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreviewComponent implements OnInit {
  @Input() gif: IGif = null;

  constructor(private sanitizer: DomSanitizer) {
  }

  get url():SafeResourceUrl {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.gif.embed_url);
  }

  ngOnInit() {
  }

}
