import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {Sort} from '../services/data.service';
import {DataService, IGif, ISearch} from '../services/data.service';

@Component({
  selector: 'app-giphy',
  templateUrl: './giphy.component.html',
  styleUrls: ['./giphy.component.scss']
})
export class GiphyComponent implements OnInit {
  gifs: IGif[] = [];

  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.handleUrlParams();

    this.dataService.gifsChanged.subscribe((newGifs) => {
      this.gifs = newGifs;
    });
  }

  sortChange(newSort: Sort) {
    this.dataService.sort = newSort;
  }

  search(query: string) {
    this.dataService.query = query;
  }


  private handleUrlParams() {
    this.activatedRoute.queryParams
      .pipe(first(params => !!Object.values(params).length))
      .subscribe((params: ISearch) => {
        this.sortChange(params.sort);
        this.search(params.query);
      });

    this.dataService.paramsChanged.subscribe((newParams) => {
      this.router.navigate(
        [],
        {
          relativeTo: this.activatedRoute,
          queryParams: newParams as any,
          queryParamsHandling: 'merge', // remove to replace all query params by provided
        });

    });


  }

}
