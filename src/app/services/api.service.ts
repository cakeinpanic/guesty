import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/internal/Observable';
import {map} from 'rxjs/operators';
import {IGif} from './data.service';

const API_KEY = 'dCpV0z0dW988CrZDZ8DYJtLMrJJI0pSz';
const LIMIT = 9;
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {
  }

  getGifs(search: string): Observable<IGif[]> {
    return this.http
      .get<{data: IGif[]}>(`https://api.giphy.com/v1/stickers/search?api_key=${API_KEY}&limit=${LIMIT}&q=${search}`)
      .pipe(map(({data}) => data));
  }
}
