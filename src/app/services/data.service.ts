import {Injectable} from '@angular/core';
import sortBy from 'lodash/sortBy';
import {Subject} from 'rxjs/internal/Subject';
import {ApiService} from './api.service';

export interface IGif {
  embed_url: string,
  title: string,
  import_datetime: string;
}

export enum Sort {
  Title = 'Title',
  Date = 'Date'
}

export interface ISearch {
  query: string,
  sort: Sort
}


@Injectable({
  providedIn: 'root'
})
export class DataService {
  private _gifs = [];

  private _sort: Sort = Sort.Title;
  private _query: string = '';

  gifsChanged = new Subject<IGif[]>();
  paramsChanged = new Subject<ISearch>();

  constructor(private api: ApiService) {
  }

  set sort(s: Sort) {
    if (this._sort === s) {
      return
    }

    this._sort = s;
    this.paramsChanged.next({query: this._query, sort: this._sort});

    this.gifsChanged.next(this.gifs);
  }

  set query(q: string) {
    if (this._query === q) {
      return
    }

    this._query = q;
    this.paramsChanged.next({query: this._query, sort: this._sort});

    if(!this._query) {
      return
    }

    this.api.getGifs(this._query).subscribe(data => {
      this.gifs = data;
    })
  }

  get gifs(): IGif[] {
    switch (this._sort) {
      case Sort.Title:
        return sortBy(this._gifs, 'title');
      case Sort.Date:
        return sortBy(this._gifs, 'import_datetime');
      default:
        return this._gifs;
    }
  }

  set gifs(gifs: IGif[]) {
    this._gifs = gifs;
    this.gifsChanged.next(this.gifs);
  }

}
